import { Component, trigger, transition, style, animate } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-about',
	templateUrl: 'about.html',
	animations: [
		trigger('slideAnimation', [
			transition(':enter', [
				style({ transform: 'translateX(100%)', opacity: 0 }),
				animate('200ms', style({ transform: 'translateX(0)', 'opacity': 1 }))
			])
		])
	]
})

export class AboutPage {
	params;
	content: Object;
	fabActive = false;

	constructor(public navCtrl: NavController, public navParams: NavParams){
		this.params = navParams.data;
		this.content = this.params.app.conteudos.shift();
	}

	changeTopic(c){
		this.navCtrl.push('AboutTabsPage', { c: c, params: this.params });
		this.fabActive = false;
	}
}
