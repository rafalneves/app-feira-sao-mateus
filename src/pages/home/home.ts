import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import * as moment from 'moment';

moment.locale("pt");

@IonicPage()
@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})

export class HomePage {
	params = null;
	events = new Array();

	constructor(public navCtrl: NavController, public navParams: NavParams){
		this.params = navParams.data;

		var d = new Date();
		for(var i = 0; i < this.params.agenda.length, this.events.length < 5; i++)
			if(this.params.agenda[i].hora.getTime() > d.getTime())
				this.events.push(this.params.agenda[i]);
	}
	getDayName(date){
		return moment(date).format('dddd');
	}
	getDate(date){
		return moment(date).format('DD MMMM');
	}
	details(n){
		this.navCtrl.push('NewsDetailPage', n);
	}
	eventDetails(e){
		this.navCtrl.push('AgendaDetailPage', e);
	}
}
