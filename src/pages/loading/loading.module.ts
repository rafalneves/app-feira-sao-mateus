import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';

import { RemoteServiceProvider } from '../../providers/remote-service/remote-service';

import { LoadingPage } from './loading';

@NgModule({
	declarations: [LoadingPage],
	imports: [HttpModule, IonicPageModule.forChild(LoadingPage)],
	exports: [LoadingPage],
	providers: [
		RemoteServiceProvider
	]
})
export class LoadingPageModule{}
