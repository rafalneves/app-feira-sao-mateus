import { Component } from '@angular/core';
import { NavController, ViewController, AlertController, Platform, IonicPage } from 'ionic-angular';

import { RemoteServiceProvider } from '../../providers/remote-service/remote-service';

@IonicPage()
@Component({
	selector: 'page-loading',
	templateUrl: 'loading.html'
})

export class LoadingPage {;
	app: Object;
	news: Object;
	agenda = new Array();

	error = false;

	constructor(private remoteService : RemoteServiceProvider, public navCtrl: NavController, private viewCtrl: ViewController, private alertCtrl: AlertController, private platform: Platform){
		var promises = new Array();
		promises.push(this.remoteService.getResource("/app.php").then((result) => {
			this.app = result[0];
		}).catch((error) => {
			this.error = true;
		}));
		promises.push(this.remoteService.getResource("/ws.php?tabela=noticias").then((result: any[]) => {
			this.news = result;
		}).catch((error) => {
			this.error = true;
		}));
		promises.push(this.remoteService.getResource("/ws.php?tabela=agenda").then((result: any[]) => {
			if(result && result.length){
				for(var i = 0; i < result.length; i++){
					var d = parseDate(result[i].hora);
					result[i].hora = Object.prototype.toString.call(d) === "[object Date]" && !isNaN(d.getTime()) ? d : null;
				}

				result.sort((a, b) => {
						return new Date(a.hora).getTime() - new Date(b.hora).getTime();
				});

				for(var y = 0; y < result.length; y++)
					if(result[y].destaque == 1)
						this.agenda.push(result[y]);

				for(var a = 0; a < result.length; a++)
					if(result[a].destaque == 0)
						for(var b = 0; b < this.agenda.length; b++)
							if(
								new Date(result[a].hora.getFullYear(), result[a].hora.getMonth(), result[a].hora.getDate()).getTime() ==
								new Date(this.agenda[b].hora.getFullYear(), this.agenda[b].hora.getMonth(), this.agenda[b].hora.getDate()).getTime()
							){
								if(!this.agenda[b].events)
									this.agenda[b].events = new Array();
								this.agenda[b].events.push(result[a]);

								break;
							}
			}
		}).catch((error) => {
			console.log(error)
			this.error = true;
		}));
		Promise.all(promises).then(() => {
			if(this.error)
				this.alert();
			else
				navCtrl.push('TabsPage', { app: this.app, news: this.news, agenda: this.agenda }).then(() => {
					this.viewCtrl.dismiss();
				});
		});
	}
	alert(){
		let alert = this.alertCtrl.create({
			title: "Ocorreu um erro",
			subTitle: "Erro ao carregar dados, por favor tente mais tarde.",
			buttons: [{
				text: 'Fechar',
				handler: () => {
					this.platform.exitApp();
				}
			}]
		});
		alert.present();
	}
}
function parseDate(date){
	var d = date.split('-');
	if(d.length == 4)
		return new Date(d[0], d[1] - 1, d[2], d[3].toLowerCase().replace(/ /g, '').replace(/horas/g, '').split(':')[0] || 0, d[3].toLowerCase().replace(/ /g, '').replace(/horas/g, '').split(':')[1] || 0);
	return null;
}
