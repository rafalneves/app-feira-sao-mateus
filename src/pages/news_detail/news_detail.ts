import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
	templateUrl: 'news_detail.html'
})

export class NewsDetailPage {
	n = null;
	constructor(public navCtrl: NavController, public navParams: NavParams){
		this.n = navParams.data;
	}
}
