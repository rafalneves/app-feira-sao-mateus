import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewsDetailPage } from './news_detail';

@NgModule({
	declarations: [NewsDetailPage],
	imports: [IonicPageModule.forChild(NewsDetailPage)],
	exports: [NewsDetailPage]
})
export class NewsDetailPageModule{}
