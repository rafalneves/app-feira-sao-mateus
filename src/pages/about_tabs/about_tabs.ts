import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
	templateUrl: 'about_tabs.html'
})

export class AboutTabsPage {
	c: Object;
	params: Object;

	tab1Root = 'AboutDetailPage';

	constructor(public navCtrl: NavController, public navParams: NavParams) {
		this.c = navParams.data.c;

		this.params = navParams.data.params;
	}
}
