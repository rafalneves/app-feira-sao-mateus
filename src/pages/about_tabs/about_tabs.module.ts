import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AboutTabsPage } from './about_tabs';

@NgModule({
	declarations: [AboutTabsPage],
	imports: [IonicPageModule.forChild(AboutTabsPage)],
	exports: [AboutTabsPage]
})
export class AboutTabsPageModule{}
