import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgendaDetailPage } from './agenda_detail';

@NgModule({
	declarations: [AgendaDetailPage],
	imports: [IonicPageModule.forChild(AgendaDetailPage)],
	exports: [AgendaDetailPage]
})
export class AgendaDetailPageModule{}
