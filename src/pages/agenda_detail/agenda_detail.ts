import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import * as moment from 'moment';

moment.locale("pt");

@IonicPage()
@Component({
	selector: "page-agenda_details",
	templateUrl: 'agenda_detail.html'
})

export class AgendaDetailPage {
	a: Object;

	constructor(public navCtrl: NavController, public navParams: NavParams){
		this.a = navParams.data;
	}
	getDayName(date){
		return moment(date).format('dddd');
	}
	getDate(date){
		return moment(date).format('DD [de] MMMM');
	}
	getHour(date){
		return moment(date).format('HH:mm');
	}
}
