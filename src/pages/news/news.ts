import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-news',
	templateUrl: 'news.html'
})

export class NewsPage {
	params = Object;

	constructor(public navCtrl: NavController, public navParams: NavParams){
		this.params = navParams.data;
	}
	details(n){
		this.navCtrl.push('NewsDetailPage', n);
	}
}
