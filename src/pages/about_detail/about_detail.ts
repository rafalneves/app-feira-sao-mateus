import { Component, trigger, transition, style, animate } from '@angular/core';
import { NavController, NavParams, ViewController, IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
	templateUrl: 'about_detail.html',
	animations: [
		trigger('slideAnimation', [
			transition(':enter', [
				style({ transform: 'translateX(100%)', opacity: 0 }),
				animate('200ms', style({ transform: 'translateX(0)', 'opacity': 1 }))
			])
		])
	]
})

export class AboutDetailPage {
	t: Object;
	c: Object;
	params: Object;
	fabActive = false;

	constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
		this.t = navParams.data.t;
		this.c = navParams.data.c;
		this.params = navParams.data.params;
	}

	changeTopic(cb){
		this.navCtrl.parent.parent.push('AboutTabsPage', { c: cb, params: this.params }).then(() => {
			this.navCtrl.parent.parent.remove(this.navCtrl.parent.parent.length() - 2);
		});
	}
}
