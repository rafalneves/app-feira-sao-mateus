import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import * as moment from 'moment';

moment.locale("pt");

@IonicPage()
@Component({
	selector: 'page-agenda',
	templateUrl: 'agenda.html'
})

export class AgendaPage {
	params: Object;

	constructor(public navCtrl: NavController, public navParams: NavParams){
		this.params = navParams.data;
	}
	getDayName(date){
		return moment(date).format('dddd');
	}
	getDate(date){
		return moment(date).format('DD [de] MMMM');
	}
	details(a){
		this.navCtrl.push('AgendaDetailPage', a);
	}
}
