import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
	templateUrl: 'tabs.html'
})

export class TabsPage {
	params = null;

	tab1Root = 'HomePage';
	tab2Root = 'NewsPage';
	tab3Root = 'AgendaPage';
	tab4Root = 'AboutPage';

	constructor(public navCtrl: NavController, public navParams: NavParams) {
		this.params = navParams.data;
	}
}
