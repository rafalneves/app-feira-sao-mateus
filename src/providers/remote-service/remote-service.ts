import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class RemoteServiceProvider{
	getApiUri : string = "http://www.feirasaomateus.pt";

	constructor(public http: Http){}
	getResource(resource){
		var _self = this;

		return new Promise(function(resolve, reject){
			_self.http.get(_self.getApiUri + resource).subscribe((result) => {
				if(result.status == 200)
					resolve(result.json())
				else
					reject(result);
			}, (error) => {
				reject(error);
			});
		});
	}
}
